<?php

use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\MitraController as AdminMitraController;
use App\Http\Controllers\Admin\ProductController as AdminProductController;
use App\Http\Controllers\Admin\ReviewController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MitraController;
use App\Http\Controllers\ProductController;
use Illuminate\Routing\RouteGroup;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::prefix('/')->group(function () {
    Route::resource('/', IndexController::class);
    Route::resource('blog', BlogController::class)->only('index', 'show');
    Route::get('users/{id}', [MitraController::class, 'show'])->name('mitra.show');
    Route::get('product-list', [ProductController::class, 'index'])->name('product-list');
});


Route::prefix('adm-mandip')->middleware('auth')->name('admin.')->group(function () {
    Route::resource('/', HomeController::class);
    Route::get('blog/list', [BlogController::class, 'indexAdmin'])->name('blog.list');
    Route::resource('blog', BlogController::class);
    Route::resource('product', AdminProductController::class);
    Route::resource('mitra', AdminMitraController::class);
    Route::resource('review', ReviewController::class);
});

Auth::routes();

// block register
Route::get('/register', function() {
    return redirect('/login');
});

Route::post('/register', function() {
    return redirect('/login');
});


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
