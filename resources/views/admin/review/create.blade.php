@extends('admin.master')

@section('title')
    - Tambah Review
@endsection

@section('page-title')
    Tambah Review
@endsection

@section('breadcrumb')
    Review
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.review.store') }}" enctype="multipart/form-data" method="POST">
                @csrf
                @method('POST')
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" required placeholder="Nama">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="position" class="col-sm-2 col-form-label">Posisi</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="position" name="position" required placeholder="Posisi">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="org" class="col-sm-2 col-form-label">Perusahaan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="org" name="org" placeholder="Perusahaan">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="review" class="col-sm-2 col-form-label">Review</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="review" name="review" required placeholder="Review">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div class="col-sm-10">
                            <a class="btn btn-secondary btn-tone m-r-5" href="{{ url()->previous() }}">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
