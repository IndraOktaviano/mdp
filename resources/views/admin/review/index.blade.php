@extends('admin.master')

@section('title')
    - Review Para Pelanggan
@endsection

@section('page-title')
    Review Para Pelanggan
@endsection

@section('breadcrumb')
    Review
@endsection

@section('css')
    <link href="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="{{ asset('admin/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $('#data-table').DataTable();
    </script>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row justify-content-between mb-3">
                <div class="col-auto">
                    <h3>Daftar Review</h3>
                </div>
                <div class="col-auto">
                    <a href="{{ route('admin.review.create') }}" class="btn btn-primary">Tambah Review</a>
                </div>
            </div>
            <table id="data-table" class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Organisasi</th>
                        <th>Ubah</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value => $item)
                        <tr>
                            <td>{{ $value + 1 }}</td>
                            <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->position }}</td>
                            <td>{{ $item->org }}</td>
                            <td>
                                <a href="{{ route('admin.review.edit', $item->id) }}"
                                    class="btn btn-sm btn-primary btn-tone m-r-5">Ubah</a>
                            </td>
                            <td>
                                <form onsubmit="return confirm('Anda akan menghapus {{ $item->name }}?')"
                                    action="{{ route('admin.review.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger btn-tone m-r-5">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Organisasi</th>
                        <th>Ubah</th>
                        <th>Hapus</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
