@extends('admin.master')

@section('title')
    - {{ $data->name }}
@endsection

@section('page-title')
    Ubah Product
@endsection

@section('breadcrumb')
    {{ $data->name }}
@endsection

@section('js')
    <script>
        document.querySelector('.custom-file-input').addEventListener('change', function(e) {
            var fileName = document.getElementById("customFile").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = fileName
        })
    </script>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.product.update', $data->id) }}" enctype="multipart/form-data" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama Produk</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}" required placeholder="Nama Produk">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price" class="col-sm-2 col-form-label">Harga (Opsional) </label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="price" name="price" value="{{$data->price}}" placeholder="Harga">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="customFile" class="col-sm-2 col-form-label">Gambar</label>
                    <div class="col-sm-10">
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">{{$data->image}}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2">Status</div>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox" name="status"  {{ $data->status != null ? 'checked' : '' }} id="gridCheck1">
                            <label for="gridCheck1">
                                Aktif
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div class="col-sm-10">
                            <a class="btn btn-secondary btn-tone m-r-5" href="{{ url()->previous() }}">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
