@extends('admin.master')

@section('title')
    - Blog
@endsection

@section('page-title')
    Blog
@endsection

@section('breadcrumb')
    Blog
@endsection

@section('css')
    <link href="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">

    <style>
        .td-title {
            width: 55vh;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            color: #5A5A66;
        }

    </style>
@endsection

@section('js')
    <script src="{{ asset('admin/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $('#data-table').DataTable();
    </script>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row justify-content-between mb-3">
                <div class="col-auto">
                    <h3>Daftar Blog</h3>
                </div>
                <div class="col-auto">
                    <a href="{{ route('admin.blog.create') }}" class="btn btn-primary">Buat baru</a>
                </div>
            </div>
            <table id="data-table" class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Status</th>
                        <th>Ubah</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value => $item)
                        <tr>
                            <td>{{ $value + 1 }}</td>
                            <td>{{ date('d-m-Y', strtotime($item->created_at)); }}</td>
                            <td>
                                <a href="{{route('blog.show', $item->slug)}}" target="_blank" class="td-title">{{ $item->title }}</a>
                            </td>
                            <td>
                                @if ($item->status != null)
                                    <span class="badge badge-pill badge-info">Show</span>
                                @else
                                    <span class="badge badge-pill badge-warning">Hidden</span>
                                @endif
                            </td>
                            <td><a href="{{ route('admin.blog.edit', $item->id) }}"
                                    class="btn btn-sm btn-primary btn-tone m-r-5">Ubah</a></td>
                            <td>
                                <form onsubmit="return confirm('Anda akan menghapus {{ $item->title }}?')"
                                    action="{{ route('admin.blog.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger btn-tone m-r-5">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Judul</th>
                        <th>Status</th>
                        <th>Ubah</th>
                        <th>Hapus</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
