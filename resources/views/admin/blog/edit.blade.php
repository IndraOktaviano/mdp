@extends('admin.master')

@section('title')
    - {{ $data->title }}
@endsection

@section('page-title')
    Edit Blog
@endsection

@section('breadcrumb')
    edit blog : {{ $data->title }}
@endsection

@section('css')
    <link href="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">
@endsection

@section('js')
    <script src="https://cdn.tiny.cloud/1/fk6mrr5194u57okny3axnk6x9a0f6oxe74z9ufemtokfslbi/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
            toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
        });
    </script>

    <script>
        document.querySelector('.custom-file-input').addEventListener('change', function(e) {
            var fileName = document.getElementById("customFile").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = fileName
        })
    </script>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.blog.update', $data->id) }}" enctype="multipart/form-data" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group row">
                    <label for="title" class="col-sm-2 col-form-label">Judul</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $data->title }}" class="form-control" id="title" name="title"
                            placeholder="Judul">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="desc" class="col-sm-2 col-form-label">Deskripsi</label>
                    <div class="col-sm-10">
                        <textarea name="desc" id="desc" cols="30" rows="10">
                                        {{ $data->desc }}
                                    </textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="customFile" class="col-sm-2 col-form-label">Gambar</label>
                    <div class="col-sm-10">
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="customFile">
                            <label class="custom-file-label" for="customFile">{{ $data->image }}</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2">Status</div>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <input type="checkbox" name="status" {{ $data->status != null ? 'checked' : '' }}
                                id="gridCheck1">
                            <label for="gridCheck1">
                                Aktif
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div class="col-sm-10">
                            <a class="btn btn-secondary btn-tone m-r-5" href="{{ url()->previous() }}">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
