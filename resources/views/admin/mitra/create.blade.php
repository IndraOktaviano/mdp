@extends('admin.master')

@section('title')
    - Tambah Mitra
@endsection

@section('page-title')
    Tambah Mitra
@endsection

@section('breadcrumb')
    Tambah Mitra
@endsection

@section('js')
    <script src="https://cdn.tiny.cloud/1/fk6mrr5194u57okny3axnk6x9a0f6oxe74z9ufemtokfslbi/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
            toolbar: 'undo redo | blocks fontfamily fontsize | bold italic underline strikethrough | link image media table | align lineheight | numlist bullist indent outdent | emoticons charmap | removeformat',
        });
    </script>
    <script>
        document.querySelector('.custom-file-input').addEventListener('change', function(e) {
            var fileName = document.getElementById("customFile").files[0].name;
            var nextSibling = e.target.nextElementSibling
            nextSibling.innerText = fileName
        })
    </script>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('admin.mitra.store') }}" enctype="multipart/form-data" method="POST">
                @csrf
                @method('POST')
                <div class="form-group row">
                    <label for="name" class="col-sm-2 col-form-label">Nama Mitra</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name" required placeholder="Nama Mitra">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="customFile" class="col-sm-2 col-form-label">Logo</label>
                    <div class="col-sm-10">
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" required id="customFile">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="content" class="col-sm-2 col-form-label">Konten</label>
                    <div class="col-sm-10">
                        <textarea name="content" id="content" cols="30" rows="15"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-10">
                        <div class="col-sm-10">
                            <a class="btn btn-secondary btn-tone m-r-5" href="{{ url()->previous() }}">Kembali</a>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
