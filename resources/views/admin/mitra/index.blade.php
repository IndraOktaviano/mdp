@extends('admin.master')

@section('title')
    - Partner / Mitra
@endsection

@section('page-title')
    Partner / Mitra
@endsection

@section('breadcrumb')
    Partner / Mitra
@endsection

@section('css')
    <link href="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.css') }}" rel="stylesheet">

    <style>
        table img {
            width: 80px;
        }
    </style>
@endsection

@section('js')
    <script src="{{ asset('admin/vendors/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('admin/vendors/datatables/dataTables.bootstrap.min.js') }}"></script>

    <script>
        $('#data-table').DataTable();
    </script>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row justify-content-between mb-3">
                <div class="col-auto">
                    <h3>Daftar Mitra</h3>
                </div>
                <div class="col-auto">
                    <a href="{{ route('admin.mitra.create') }}" class="btn btn-primary">Tambah Mitra</a>
                </div>
            </div>
            <table id="data-table" class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Gambar</th>
                        <th>Ubah</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $value => $item)
                        <tr>
                            <td>{{ $value + 1 }}</td>
                            <td>{{ date('d-m-Y', strtotime($item->created_at)) }}</td>
                            <td>{{ $item->name }}</td>
                            <td>
                                <img src="{{ asset('images/available/'.$item->image) }}" alt="">
                            </td>
                            <td>
                                <a href="{{ route('admin.mitra.edit', $item->id) }}"
                                    class="btn btn-sm btn-primary btn-tone m-r-5">Ubah</a>
                            </td>
                            <td>
                                <form onsubmit="return confirm('Anda akan menghapus {{ $item->name }}?')"
                                    action="{{ route('admin.mitra.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger btn-tone m-r-5">Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Nama</th>
                        <th>Gambar</th>
                        <th>Ubah</th>
                        <th>Hapus</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
