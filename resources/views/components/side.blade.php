<div class="side-nav">
    <div class="side-nav-inner">
        <ul class="side-nav-menu scrollable">
            <li class="nav-item dropdown {{ (request()->is('adm-mandip/product*')) ? 'active' : ''}}">
                <a href="{{ route('admin.product.index') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-shopping"></i>
                    </span>
                    <span class="title">Product</span>
                </a>
            </li>
            <li class="nav-item dropdown {{ (request()->is('adm-mandip/blog*')) ? 'active' : ''}}">
                <a href="{{ url('adm-mandip/blog/list') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-bold"></i>
                    </span>
                    <span class="title">Blog</span>
                </a>
            </li>
            <li class="nav-item dropdown {{ (request()->is('adm-mandip/mitra*')) ? 'active' : ''}}">
                <a href="{{ route('admin.mitra.index') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-team"></i>
                    </span>
                    <span class="title">Partner/ Mitra</span>
                </a>
            </li>
            <li class="nav-item dropdown {{ (request()->is('adm-mandip/review*')) ? 'active' : ''}}">
                <a href="{{ route('admin.review.index') }}">
                    <span class="icon-holder">
                        <i class="anticon anticon-star"></i>
                    </span>
                    <span class="title">Review</span>
                </a>
            </li>
        </ul>
    </div>
</div>
