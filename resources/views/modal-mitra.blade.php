<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{$item->name}}</h4>
    </div>
    <div class="modal-body" style="text-align: start">
        <img src="{{ asset('images/available/' . $item->image) }}" style="opacity: 1; width: 50%">
        @if ($item->content != null)
            <div class="space20"></div>
        @endif
        {!!$item->content!!}
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div><!-- /.modal-content -->
