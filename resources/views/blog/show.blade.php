<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>{{ $blog->title }} | Mandiri Dayyan Pratama</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="{{ $blog->title }}">
    <meta name="keywords" content="">
    <meta name="author" content="Mandiri Dayyan Pratama">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- ICONS -->
    <link rel="stylesheet" href="{{ asset('css/ilmosys-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/fontawesome/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/icon2/style.css') }}">
    <link rel="stylesheet" href="{{ asset('js/vendors/swipebox/css/swipebox.min.css') }}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


    <!-- THEME / PLUGIN CSS -->
    <link rel="stylesheet" href="{{ asset('js/vendors/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style>
        .hero-blog {
            padding: 250px 0 250px;
            margin-bottom: 40px;
            margin-top: 99px;
        }

        h1 {
            color: white;
        }

        .navbar-inverse {
            background: white !important;
        }
    </style>

</head>

<body id="home">
    <div class="body">
        <!-- HEADER -->
        <header>
            <nav class="navbar-inverse navbar-lg navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url('/') }}" class="navbar-brand brand"><img
                                src="{{ asset('images/logo.png') }}" alt="logo"></a>
                    </div>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right navbar-login">
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=6281218684243&text=Halo%20admin%20Mandiri%20Dayyan%20Pratama..."
                                    target="_blank">Hubungi Kami</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">


                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="{{ url('/') }}#home">Home</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="{{ url('/') }}#about">About</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="{{ url('/') }}#feature">Feature</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="{{ url('/') }}#collection">Product</a>
                            </li>
                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="{{ url('/') }}#faq">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <!-- Hero Header -->
        <div class="intro hero-blog"
            style="background: linear-gradient(rgba(0, 0, 0, 0.713), rgba(0, 0, 0, 0.713)), url('{{ asset('images/blog/' . $blog->image) }}'); background-size: cover; background-position: center; background-repeat: no-repeat;">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 text-center">
                        <h1>{{ $blog->title }}</h1>
                    </div>
                </div>
            </div>
        </div>

        <!-- Collection -->
        <div id="collection">
            <div class="container">
                {!! $blog->desc !!}
            </div>

            <div class="clearfix"></div>
            <div class="space100"></div>

            <!-- FAQ -->
            <div id="faq" class="bg-light">

                <!-- FOOTER -->
                <footer class="footer2" id="footer2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 footerP">
                                <a href="index.html" class="footer-logo"><img src="{{ asset('images/logo.png') }}"
                                        alt="logo"></a>
                            </div>
                            <div class="col-md-2 footerP">
                                <a href="#">
                                    <p>Cookies</p>
                                </a>
                            </div>
                            <div class="col-md-2 footerP">
                                <a href="#">
                                    <p>Privacy Policy</p>
                                </a>
                            </div>
                            <div class="col-md-2 footerP">
                                <a href="#">
                                    <p>Blog</p>
                                </a>
                            </div>
                            <div class="col-md-3 footerP">
                                <div class="footer-social pull-right">
                                    <a href="#" class="fa fa-facebook"></a>
                                    <a href="#" class="fa fa-twitter"></a>
                                    <a href="#" class="fa fa-instagram"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

                <!-- COPYRIGHT -->
                <div class="footer-copy">
                    <div class="container">
                        &copy; 2020. Protem. All rights reserved.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT =============================-->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/vendors/slick/slick.min.js') }}"></script>
    <script src="{{ asset('js/vendors/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/vendors/stellar.js') }}"></script>
    <script src="{{ asset('js/vendors/isotope/isotope.pkgd.js') }}"></script>
    <script src="{{ asset('js/vendors/swipebox/js/jquery.swipebox.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/vendors/mc/jquery.ketchup.all.min.js') }}"></script>
    <script src="{{ asset('js/vendors/mc/main.js') }}"></script>
    <script src="{{ asset('js/vendors/contact.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('js/vendors/gmap.js') }}"></script>

</body>

</html>
