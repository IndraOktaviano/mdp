<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <title>Mandiri Dayyan Pratama </title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Mandiri Dayyan Pratama">

    <!-- FAVICON -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- ICONS -->
    <link rel="stylesheet" href="{{ asset('css/ilmosys-icon.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/fontawesome/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/icon2/style.css') }}">
    <link rel="stylesheet" href="{{ asset('js/vendors/swipebox/css/swipebox.min.css') }}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">


    <!-- THEME / PLUGIN CSS -->
    <link rel="stylesheet" href="{{ asset('js/vendors/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
 <![endif]-->

</head>

<body id="home">
    <div class="body">
        <!-- HEADER -->
        <header>
            <nav class="navbar-inverse navbar-lg navbar-fixed-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="{{ url('/') }}" class="navbar-brand brand"><img
                                src="{{ asset('images/logo.png') }}" alt="logo"></a>
                    </div>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right navbar-login">
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=6281218684243&text=Halo%20admin%20Mandiri%20Dayyan%20Pratama..."
                                    target="_blank">Hubungi Kami</a>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">


                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="#home">Home</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="#about">About</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="#feature">Feature</a>
                            </li>

                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="#collection">Product</a>
                            </li>
                            <li class="dropdown mm-menu">
                                <a class="page-scroll" href="#faq">FAQ</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <!-- Hero Header -->
        <div class="intro intro1">
            <div class="container">
                <div class="row center-content">
                    <div class="col-md-6">
                        <h2>Bergabunglah dengan kami </h2>
                        <p>Mandiri Dayyan Pratama merupakan perusahaan yang bergerak di bidang pengadaan barang dan jasa
                            dalam memenuhi seluruh kebutuhan bisnis anda dan kami hadir dan berkomitmen untuk menjawab
                            seluruh kebutuhan bisnis anda secara efektif dan efisien.</p>
                        <div class="space30"></div>
                        {{-- <a href="https://www.youtube.com/watch?v=bZx8rPd-PKQ"
                            class="swipebox-video btn btn-lg btn-ylw">Play Video <i class="icon-arrow-right"></i></a> --}}
                    </div>
                    <br><br>
                    <div class="col-sm-4 col-md-7">
                        <img src="{{ asset('images/feature/pic-1.jpg') }}" class="img-responsive img-rounded"
                            alt="" />
                    </div>
                </div>
            </div>
        </div>

        <!-- ABOUT -->
        <div id="about" class="container">
            <div class="space100"></div>
            <!-- INFO CONTENT -->
            <div class="info-content">
                <div class="container">
                    <div class="row center-content">
                        <div class="col-md-6">
                            <h3>Tentang Kami </h3>
                            <img src="{{ asset('images/line.png') }}" class="img-responsive" alt="" /><br>
                            <p>
                                Mandiri Dayyan Pratama merupakan perusahaan yang bergerak di bidang pengadaan barang dan
                                jasa dalam memenuhi seluruh kebutuhan bisnis anda dan kami hadir dan berkomitmen untuk
                                menjawab kebutuhan bisnis anda secara efektif dan efisien.
                                <br>
                                Untuk itu kami berikan kesempatan kepada customer yang tentunya layanan terbaik dalam
                                memenuhi seluruh requirement kebutuhan barang dan jasa.
                            </p>
                            <ul class="list">
                                <li><i class="icon-check"></i> Profesional.</li>
                                <li><i class="icon-check"></i> Terpercaya.</li>
                                <li><i class="icon-check"></i> Berkembang.</li>
                            </ul>
                            <div class="space30"></div>
                            <button type="button" class="btn btn-lg btn-default" data-toggle="modal"
                                data-target="#aboutModal">
                                Selengkapnya <i class="icon-arrow-right"></i>
                            </button>
                            {{-- <a href="#" class="btn btn-lg btn-default">Learn More <i class="icon-arrow-right"></i></a> --}}

                            <div class="modal fade" id="aboutModal" tabindex="-1" role="dialog"
                                aria-labelledby="aboutModalLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="aboutModalLabel">Tentang Kami</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h4>
                                                <strong>PT Mandiri Dayyan Pratama</strong>
                                            </h4>
                                            <p>
                                                Mandiri Dayyan Pratama merupakan perusahaan yang bergerak di bidang
                                                pengadaan barang dan jasa dalam memenuhi seluruh kebutuhan bisnis anda
                                                dan kami hadir dan berkomitmen untuk menjawab seluruh kebutuhan bisnis
                                                anda secara efektif dan efisien.
                                            </p>
                                            <p>
                                                Untuk itu kami berikan kesempatan kepada customer yang tentunya layanan
                                                terbaik dalam memenuhi seluruh requirement kebutuhan barang dan jasa.
                                            </p>
                                            <p>
                                                Selain pengadaan barang kamipun juga mensupport kebutuhan jasa yang
                                                tentunya jasa teknologi berupa pembuatan aplikasi yang tentunya dapat
                                                membuat dan mempermudah cutomer dalam melakukan aktivitas bisnis dalam
                                                usahan anda.
                                            </p>
                                            <p>
                                                Mandiri Dayyan Pratama selalu memberikan layanan pengadaan barang dan
                                                jasa serta purna jual yang terbaik kepada setiap customer sesuai dengan
                                                standar operasional prosedur yang yang ada.
                                            </p>
                                            <p class="blue">
                                                “Kesuksesaan bisnis ini adalah memberikan layanan yang terbaik serta
                                                focus pada kebutuhan pelanggan”
                                            </p>
                                            <p>
                                                Mandiri Dayyan Pratama merupakan perusahaan yang bergerak di bidang
                                                pengadaan barang dan jasa dalam memenuhi seluruh kebutuhan bisnis anda
                                                dan kami hadir dan berkomitmen untuk menjawab kebutuhan bisnis anda
                                                secara efektif dan efisien.
                                            </p>
                                            <p>
                                                Untuk itu kami berikan kesempatan kepada customer yang tentunya layanan
                                                terbaik dalam memenuhi seluruh requirement kebutuhan barang dan jasa.
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default"
                                                data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 text-center">
                            <img src="{{ asset('images/feature/pic-2.jpg') }}"
                                class="pull-right img-responsive img-rounded" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
        </div>
        <div class="space100"></div>

        <!-- SERVICES -->
        <section id="feature" class="services bg-light">
            <div class="container">
                <div class="about-inline text-center">
                    <h3>Layanan Kami </h3>
                    <img src="{{ asset('images/line.png') }}" class="img-responsive center-block" alt="" />
                    <p>Fokus bisnis kami adalah melayani pengadaan barang-barang yang mensupport kebutuhan customer
                        dalam hal ini adalah mechanical electrical serta pengadaan jasa pembuatan aplikasi yang sesuai
                        dengan kebutuhan customer.</p>
                </div>
                <div class="container">
                    <div class="row">
                        <!-- Column -->
                        <div class="col-md-6 Protem-feature">
                            <div class="box-body d-flex">
                                <div class="feature-icon">
                                    <img src="{{ asset('images/feature/Gift.png') }}" style="width: 100%">
                                </div>
                                <div class="Protem-feature">
                                    <a href="#">
                                        <h3>Layanan Pengadaan barang</h3>
                                    </a>
                                    <p>Seluruh produk kami adalah mengadakan layanan pengadaan barang terkait kebutuhan
                                        untuk mechanical electrical.</p>
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- Column -->
                            <div class="col-md-6 Protem-feature">
                                <div class="box-body d-flex">
                                    <div class="feature-icon">
                                        <img src="{{ asset('images/feature/Electronics 1.png') }}" style="width: 100%">
                                    </div>
                                    <div class="Protem-feature">
                                        <a href="#">
                                            <h3>Layanan pengadaan jasa teknologi</h3>
                                        </a>
                                        <p>Salah satu support kami juga melayani pengadaan pembuatan aplikasi yang
                                            mendukung kebutuhan bisnis customer.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- PARALLAX -->
        <section class="parallax-content text-center" data-stellar-background-ratio="0.4">
            <div class="container">
                <div class="row">
                    <div class="col-md-10">
                        <h4><strong>Tertarik dengan layanan kami?</strong></h4>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <a href="https://api.whatsapp.com/send?phone=6281218684243&text=Halo%20admin%20Mandiri%20Dayyan%20Pratama..."
                                target="_blank" class="btn btn-lg btn-primary pull-right">Hubungi Kami <i
                                    class="icon-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- benifits 1 -->
        <div class="intro intro-why">

            <div class="row center-content">

                <div class="col-md-6">
                    <img src="images/feature/why.jpg" class="pull-left" alt="" />
                </div>

                <div class="container">
                    <div class="col-md-9 why col-md-push-1">
                        <h2>Mengapa harus kami?</h2>
                        <img src="images/line.png" class="img-responsive" alt="" /><br />
                        <p>Kesuksesaan bisnis ini adalah memberikan layanan yang terbaik serta focus pada kebutuhan
                            pelanggan</p>
                        <br />
                        <ul class="list">
                            <li><i class="icon-check"></i> Terpercaya</li>
                            <li><i class="icon-check"></i> Selalu Berkembang</li>
                            <li><i class="icon-check"></i> Profesional</li>
                            <li><i class="icon-check"></i> Melayani dengan senyuman</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <!-- Collection -->
        <div id="collection">
            <div class="container">
                <div class="about-inline text-center">
                    <h3>Produk dan Layanan</h3>
                    <img src="images/line.png" class="img-responsive center-block" alt="" />
                    <p>Daftar produk dan layanan kami</p>
                </div>

                <div class="row">
                    @foreach ($product as $item)
                        @if ($item->status == 'on')
                            <div class="col-md-4">
                                <div class="card card-product">
                                    <div class="img-wrap"><img
                                            src="{{ asset('images/products/' . $item->image) }}" alt="img"></div>
                                    <div class="product-info">
                                        <h4>{{ $item->name }}</h4>
                                        <div class="price-wrap h3">
                                            <a href="https://api.whatsapp.com/send?phone=6281218684243&text=Halo%20admin%20Mandiri%20Dayyan%20Pratama%20saya%20ingin%20memesan%20{{ $item->name }}"
                                                target="_blank" class="btn btn-sm btn-default pull-left">Hubungi
                                                Kami</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="col-md-12 text-right">
                        <a href="" class="btn btn-lg btn-primary">Selengkapnya <i class="icon-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <!-- Collection -->

            <div class="space100"></div>
            <!-- review -->
            <div class="review products-review">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="review-text">
                                @foreach ($review as $item)
                                <div>
                                    <p>
                                        "{{$item->review}}"
                                    </p>
                                    <img src="./images/bg/five-star.png" alt="img">
                                    <span class="author">{{$item->name}} <br> <small> {{$item->position}}
                                        </small></span>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="review-title col-md-12">
                        <h1>REVIEW</h1>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="space100"></div>

            <!-- FAQ -->
            <div id="faq" class="bg-light">
                <div class="container">
                    <div class="about-inline text-center">
                        <h3>FAQ</h3>
                        <img src="images/line.png" class="img-responsive center-block" alt="" />
                        {{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam et purus a odio finibus
                            bibendum in sit amet leo. Mauris feugiat erat tellus.</p> --}}
                    </div>

                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="images/feature/faq.jpg" class="img-responsive pull-left img-rounded"
                                    alt="" />
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-10 col-md-offset-1 elements-content">
                                    <div class="clearfix"></div>
                                    <div class="panel-group" id="accordion" role="tablist"
                                        aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                                        href="#collapseOne" aria-expanded="true"
                                                        aria-controls="collapseOne">
                                                        Apa yang bisa dapat MDP bantu ?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                                aria-labelledby="headingOne">
                                                <div class="panel-body">
                                                    Anda dapat bertanya terkait apapun tentang layanan mdp yang tentunya
                                                    akan kami jawab sesuai dengan kapasitas kami
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse"
                                                        data-parent="#accordion" href="#collapseTwo"
                                                        aria-expanded="false" aria-controls="collapseTwo">
                                                        Bagaimana saya mendapatkan informasi tentang MDP ?
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                                aria-labelledby="headingTwo">
                                                <div class="panel-body">
                                                    <ol type="a">
                                                        <li>
                                                            Anda dapat menghubungi via chat / Whatapps melalui nomor <a
                                                                href="https://api.whatsapp.com/send?phone=6281218684243&text=Halo%20admin%20Mandiri%20Dayyan%20Pratama..."
                                                                target="_blank"
                                                                rel="noopener noreferrer">0812-1868-4243</a>
                                                        </li>
                                                        <br>
                                                        <li>
                                                            Anda dapat menggunakan layanan email ke <a
                                                                href="mailto:info@mdpratama.co.id"
                                                                target="_blank">info@mdpratama.co.id</a>
                                                        </li>
                                                    </ol>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                        <div class="space100"></div>
                    </div>
                </div>
                <!-- FAQ -->

                <!-- AVAILABLE -->
                <div class="elements-content bg-available">
                    <div class="container">
                        <div class="row">
                            <ul class="clients no-padding text-center">
                                <li>
                                    <h1 class="text-left">Partner Kami</h1>
                                </li>
                                @foreach ($mitra as $item)
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#mitraModal{{$item->id}}">
                                            <img src="{{ asset('images/available/' . $item->image) }}"
                                                class="img-responsive center-block img-rounded"
                                            alt="{{ $item->name }}" />
                                        </a>
                                    </li>

                                    <div class="modal fade" id="mitraModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="mitraModalLabel">
                                        <div class="modal-dialog" role="document">
                                            @include('modal-mitra')
                                        </div><!-- /.modal-dialog -->
                                    </div><!-- /.modal -->
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- FOOTER -->
                <footer class="footer2" id="footer2">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 footerP">
                                <a href="index.html" class="footer-logo"><img src="images/logo.png" alt="logo"></a>
                            </div>
                            <div class="col-md-2 footerP">
                                <a href="#">
                                    <p>Cookies</p>
                                </a>
                            </div>
                            <div class="col-md-2 footerP">
                                <a href="#">
                                    <p>Privacy Policy</p>
                                </a>
                            </div>
                            <div class="col-md-2 footerP">
                                <a href="{{ route('blog.index') }}">
                                    <p>Blog</p>
                                </a>
                            </div>
                            <div class="col-md-3 footerP">
                                <div class="footer-social pull-right">
                                    <a href="#" class="fa fa-facebook"></a>
                                    <a href="#" class="fa fa-twitter"></a>
                                    <a href="#" class="fa fa-instagram"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>

                <!-- COPYRIGHT -->
                <div class="footer-copy">
                    <div class="container">
                        &copy; 2022 Mandiri Dayyan Pratama.
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" data-toggle="myModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header text-center">

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <div class="cf2-wrap">
                                <h2>Make Your Order Now!</h2>
                                <div class="clearfix space40"></div>
                                <form class="positioned" name="sentMessage" id="contactForm"
                                    action="php/contact.php" method="post">
                                    <div class="row">

                                        <div class="col-md-6 col-sm-6">
                                            <input name="senderName" id="senderName" placeholder="Your Name" required
                                                type="text">
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <input name="senderEmail" id="senderEmail" placeholder="Email Address"
                                                required type="email">
                                        </div>
                                    </div>

                                    <input name="address" id="address" placeholder="Address" required type="text">

                                    <input placeholder="Zip Code" required type="number">

                                    <input placeholder="Mobile No." required type="number">
                                    <select name="name" id="select" required="required">
                                        <option value=""> Protem Limited Edition - $175 </option>
                                        <option value="1"> Protem Limited Edition - $188 </option>
                                        <option value="2"> Protem Limited Edition - $199 </option>
                                    </select>
                                    <button type="submit" class="btn btn-primary btn-ico">Pre Order Now</button>
                                </form>
                                <div id="sendingMessage" class="statusMessage">
                                    <p><i class="fa fa-spin fa-cog"></i> Sending your message. Please wait...</p>
                                </div>
                                <div id="successMessage" class="successmessage">
                                    <p><span class="success-ico"></span> Thanks for sending your message! We'll get
                                        back to you shortly.</p>
                                </div>
                                <div id="failureMessage" class="errormessage">
                                    <p><span class="error-ico"></span> There was a problem sending your message.
                                        Please try again.</p>
                                </div>
                                <div id="incompleteMessage" class="statusMessage">
                                    <p><i class="fa fa-warning"></i> Please complete all the fields in the form before
                                        sending.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- JAVASCRIPT =============================-->
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/vendors/slick/slick.min.js') }}"></script>
    <script src="{{ asset('js/vendors/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/vendors/stellar.js') }}"></script>
    <script src="{{ asset('js/vendors/isotope/isotope.pkgd.js') }}"></script>
    <script src="{{ asset('js/vendors/swipebox/js/jquery.swipebox.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/vendors/mc/jquery.ketchup.all.min.js') }}"></script>
    <script src="{{ asset('js/vendors/mc/main.js') }}"></script>
    <script src="{{ asset('js/vendors/contact.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('js/vendors/gmap.js') }}"></script>

</body>

</html>
