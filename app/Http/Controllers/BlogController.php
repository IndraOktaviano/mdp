<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use Validator;
use File;

class BlogController extends Controller
{
    public function index()
    {
        $data['blog'] = Blog::all();
        return view('blog.index', $data);
    }

    public function indexAdmin()
    {
        $send['data'] = Blog::all();
        return view('admin.blog.index', $send);
    }

    public function create()
    {
        return view('admin.blog.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'desc' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = 'blog_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images/blog'), $name);
        };

        $data = Blog::create([
            'title' => $request->title,
            'desc' => $request->desc,
            'image' => $name,
            'status' => $request->status,
            'slug' => strtolower(str_replace(' ', '-', $request->title))
        ]);

        Blog::findOrFail($data->id)->update(['slug' => $data->slug.'-'.time()]);

        if ($data) {
            return redirect()->route('admin.blog.list')->with('message', 'Berhasil membuat blog');
        } else {
            return back()->with('message', 'Gagal membuat blog');
        }
    }

    public function show($slug)
    {
        $data['blog'] = Blog::where('slug', $slug)->first();
        return view('blog.show', $data);
    }

    public function edit($id)
    {
        $send['data'] = Blog::findOrFail($id);
        return view('admin.blog.edit', $send);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'desc' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        $data = Blog::findOrFail($id);

        if ($request->hasFile('image') == "") {
            $data->update([
                'title' => $request->title,
                'desc' => $request->desc,
                'status' => $request->status,
                'slug' => strtolower(str_replace(' ', '-', $request->title))
            ]);
            Blog::findOrFail($data->id)->update(['slug' => $data->slug.'-'.$data->id]);
        } else {
            // hapus file lama
            $path = public_path("images/blog/").$data->image;
            if (File::exists($path)) {
                unlink($path);
            }

            // tambah file baru
            $image = $request->file('image');
            $name = 'blog_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/blog'), $name);

            $data->update([
                'title' => $request->title,
                'desc' => $request->desc,
                'status' => $request->status,
                'image' => $name,
                'slug' => strtolower(str_replace(' ', '-', $request->title))
            ]);
            Blog::findOrFail($data->id)->update(['slug' => $data->slug.'-'.time()]);
        }

        if ($data) {
            return redirect()->route('admin.blog.list')->with('message', 'Blog berhasil diperbaharui');
        } else {
            return back()->with('message', 'Blog gagal diperbaharui');
        }
    }

    public function destroy($id)
    {
        $data = Blog::findOrFail($id);
        $path = public_path("images/blog/").$data->image;
        if (File::exists($path)) {
            unlink($path);
        }
        $data->delete();
        return back()->with('message', 'Blog telah dihapus');
    }
}
