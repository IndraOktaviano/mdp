<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function index()
    {
        $data['data'] = Review::all();
        return view('admin.review.index', $data);
    }

    public function create()
    {
        return view('admin.review.create');
    }

    public function store(Request $request)
    {
        $data = Review::create([
            'name' => $request->name,
            'org' => $request->org,
            'position' => $request->position,
            'review' => $request->review,
        ]);

        Review::findOrFail($data->id);

        if ($data) {
            return redirect()->route('admin.review.index')->with('message', 'Berhasil menambahkan review');
        } else {
            return back()->with('message', 'Gagal menambahkan review');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $data['data'] = Review::findOrFail($id);
        return view('admin.review.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $data = Review::findOrFail($id);

        $data->update([
            'name' => $request->name,
            'org' => $request->org,
            'position' => $request->position,
            'review' => $request->review,
        ]);
        Review::findOrFail($data->id);

        if ($data) {
            return redirect()->route('admin.review.index')->with('message', 'Review berhasil diperbaharui');
        } else {
            return back()->with('message', 'Review gagal diperbaharui');
        }
    }

    public function destroy($id)
    {
        $data = Review::findOrFail($id);
        $data->delete();
        return back()->with('message', 'Review telah dihapus');
    }
}
