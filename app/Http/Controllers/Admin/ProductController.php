<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use File;
use Illuminate\Http\Request;
use Validator;

class ProductController extends Controller
{
    public function index()
    {
        $send['data'] = Product::all();
        return view('admin.product.index', $send);
    }

    public function create()
    {
        return view('admin.product.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = $request->name . '_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images/products'), $name);
        };

        $data = Product::create([
            'name' => $request->name,
            'price' => $request->price,
            'image' => $name,
            'status' => $request->status,
            'slug' => strtolower(str_replace(' ', '-', $request->title))
        ]);

        Product::findOrFail($data->id);

        if ($data) {
            return redirect()->route('admin.product.index')->with('message', 'Berhasil menambahkan produk');
        } else {
            return back()->with('message', 'Gagal menambahkan produk');
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $send['data'] = Product::findOrFail($id);
        return view('admin.product.edit', $send);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'desc' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        $data = Product::findOrFail($id);

        if ($request->hasFile('image') == "") {
            $data->update([
                'name' => $request->name,
                'price' => $request->price,
                'status' => $request->status,
            ]);
            Product::findOrFail($data->id);
        } else {
            // hapus file lama
            $path = public_path("images/products/").$data->image;
            if (File::exists($path)) {
                unlink($path);
            }

            // tambah file baru
            $image = $request->file('image');
            $name = $request->name . '_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/products'), $name);

            $data->update([
                'name' => $request->name,
                'price' => $request->price,
                'status' => $request->status,
                'image' => $name,
            ]);
            Product::findOrFail($data->id);
        }

        if ($data) {
            return redirect()->route('admin.product.index')->with('message', 'Product berhasil diperbaharui');
        } else {
            return back()->with('message', 'Product gagal diperbaharui');
        }
    }

    public function destroy($id)
    {
        $data = Product::findOrFail($id);
        $path = public_path("images/products/").$data->image;
        if (File::exists($path)) {
            unlink($path);
        }
        $data->delete();
        return back()->with('message', 'Produk telah dihapus');
    }
}
