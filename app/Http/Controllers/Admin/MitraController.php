<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Mitra;
use Illuminate\Http\Request;
use Validator;
use File;

class MitraController extends Controller
{
    public function index()
    {
        $data['data'] = Mitra::all();
        return view('admin.mitra.index', $data);
    }

    public function create()
    {
        return view('admin.mitra.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        if($request->hasFile('image')){
            $file = $request->file('image');
            $name = $request->name . '_' . time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('images/available'), $name);
        };

        $data = Mitra::create([
            'name' => $request->name,
            'image' => $name,
            'content' => $request->content
        ]);

        Mitra::findOrFail($data->id);

        if ($data) {
            return redirect()->route('admin.mitra.index')->with('message', 'Berhasil menambahkan partner/mitra');
        } else {
            return back()->with('message', 'Gagal menambahkan partner/mitra');
        }
    }

    public function show(Mitra $mitra)
    {
        //
    }

    public function edit($id)
    {
        $data['data'] = Mitra::findOrFail($id);
        return view('admin.mitra.edit', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'image' => ['required|mimes:jpg, jpeg, png'],
        ]);

        $data = Mitra::findOrFail($id);

        if ($request->hasFile('image') == "") {
            $data->update([
                'name' => $request->name,
                'content' => $request->content
            ]);
            Mitra::findOrFail($data->id);
        } else {
            // hapus file lama
            $path = public_path("images/available/").$data->image;
            if (File::exists($path)) {
                unlink($path);
            }

            // tambah file baru
            $image = $request->file('image');
            $name = $request->name . '_' . time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images/available'), $name);

            $data->update([
                'name' => $request->name,
                'image' => $name,
                'content' => $request->content
            ]);
            Mitra::findOrFail($data->id);
        }

        if ($data) {
            return redirect()->route('admin.mitra.index')->with('message', 'Mitra berhasil diperbaharui');
        } else {
            return back()->with('message', 'Mitra gagal diperbaharui');
        }
    }

    public function destroy($id)
    {
        $data = Mitra::findOrFail($id);
        $path = public_path("images/available/").$data->image;
        if (File::exists($path)) {
            unlink($path);
        }
        $data->delete();
        return back()->with('message', 'Mitra telah dihapus');
    }
}
