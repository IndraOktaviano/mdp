<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use Illuminate\Http\Request;

class MitraController extends Controller
{
    public function show($id)
    {
        $data['mitra'] = Mitra::findOrFail($id);
        return view('mitra.show', $data);
    }
}
